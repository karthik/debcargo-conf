rust-trust-dns-client (0.22.0-5) unstable; urgency=medium

  * Team upload.
  * Package trust-dns-client 0.22.0 from crates.io using debcargo 2.6.0
  * Upload to unstable.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 07 Nov 2023 19:28:57 +0000

rust-trust-dns-client (0.22.0-4) experimental; urgency=medium

  * Team upload.
  * Package trust-dns-client 0.22.0 from crates.io using debcargo 2.6.0
  * Add upstream patch for ring 0.17.
  * Bump trust-dns-proto dependency to ensure both packages are built against
    the same version of ring, even if a semver-suffix package is introduced.
  * Disable env-filter feature in tracing-subscriber dev-depdency, it doesn't
    seem to be needed, and is not currently available in Debian.
    (see bug #1053420)

 -- Peter Michael Green <plugwash@debian.org>  Sat, 04 Nov 2023 15:41:23 +0000

rust-trust-dns-client (0.22.0-3) unstable; urgency=medium

  * Team upload.
  * Package trust-dns-client 0.22.0 from crates.io using debcargo 2.6.0
  * Bump rustls dependency to 0.21
  * Add depends on librust-trust-dns-proto-dev (>= 0.22.0-4) to ensure
    trust-dns-proto and trust-dns-client are built against the same version
    of rustls.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 17 Aug 2023 15:15:07 +0000

rust-trust-dns-client (0.22.0-2) unstable; urgency=medium

  * Package trust-dns-client 0.22.0 from crates.io using debcargo 2.6.0
  * Enable quic support

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 10 Feb 2023 09:34:09 -0500

rust-trust-dns-client (0.22.0-1) unstable; urgency=medium

  * Package trust-dns-client 0.22.0 from crates.io using debcargo 2.6.0
  * Disable quic support

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 20 Dec 2022 09:36:36 -0500

rust-trust-dns-client (0.21.2-3) unstable; urgency=medium

  * Package trust-dns-client 0.21.2 from crates.io using debcargo 2.5.0
  * disable feature 'dns-over-https-rustls'

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 04 Aug 2022 23:43:10 +0200

rust-trust-dns-client (0.21.2-2) unstable; urgency=medium

  * Package trust-dns-client 0.21.2 from crates.io using debcargo 2.5.0
  * Disable two broken tests

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 01 Aug 2022 10:30:15 +0200

rust-trust-dns-client (0.21.2-1) unstable; urgency=medium

  * Package trust-dns-client 0.21.2 from crates.io using debcargo 2.5.0

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 24 Jul 2022 08:54:34 +0200

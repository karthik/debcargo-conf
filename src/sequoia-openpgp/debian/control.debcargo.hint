Source: rust-sequoia-openpgp
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native (>= 1.67) <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-anyhow-1+default-dev (>= 1.0.18-~~) <!nocheck>,
 librust-base64-0.21+default-dev <!nocheck>,
 librust-buffered-reader-1+compression-bzip2-dev (>= 1.3.0-~~) <!nocheck>,
 librust-buffered-reader-1+compression-deflate-dev (>= 1.3.0-~~) <!nocheck>,
 librust-buffered-reader-1-dev (>= 1.3.0-~~) <!nocheck>,
 librust-bzip2-0.4+default-dev <!nocheck>,
 librust-dyn-clone-1+default-dev <!nocheck>,
 librust-flate2-1+default-dev (>= 1.0.1-~~) <!nocheck>,
 librust-idna-0.4+default-dev <!nocheck>,
 librust-lalrpop-0.20-dev <!nocheck>,
 librust-lalrpop-util-0.20+default-dev <!nocheck>,
 librust-lazy-static-1+default-dev (>= 1.4.0-~~) <!nocheck>,
 librust-libc-0.2+default-dev (>= 0.2.66-~~) <!nocheck>,
 librust-memsec-dev (<< 0.7-~~) <!nocheck>,
 librust-nettle-7+default-dev (>= 7.3-~~) <!nocheck>,
 librust-once-cell-1+default-dev <!nocheck>,
 librust-regex-1+default-dev <!nocheck>,
 librust-regex-syntax-0.8+default-dev <!nocheck>,
 librust-sha1collisiondetection-0.3+std-dev (>= 0.3.1-~~) <!nocheck>,
 librust-thiserror-1+default-dev (>= 1.0.2-~~) <!nocheck>,
 librust-xxhash-rust-0.8+default-dev <!nocheck>,
 librust-xxhash-rust-0.8+xxh3-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Alexander Kjäll <alexander.kjall@gmail.com>,
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>,
 Holger Levsen <holger@debian.org>
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/sequoia-openpgp]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/sequoia-openpgp
Homepage: https://sequoia-pgp.org/
X-Cargo-Crate: sequoia-openpgp
Rules-Requires-Root: no

Package: librust-sequoia-openpgp-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-aes-0.8+default-dev,
 librust-aes-0.8+zeroize-dev,
 librust-aes-gcm-0.10+default-dev,
 librust-aes-gcm-0.10+std-dev,
 librust-anyhow-1+default-dev (>= 1.0.18-~~),
 librust-base64-0.21+default-dev,
 librust-block-padding-0.3+default-dev,
 librust-blowfish-0.9+default-dev,
 librust-blowfish-0.9+zeroize-dev,
 librust-botan-0.10+botan3-dev (>= 0.10.6-~~),
 librust-botan-0.10+default-dev (>= 0.10.6-~~),
 librust-buffered-reader-1+compression-bzip2-dev (>= 1.3.0-~~),
 librust-buffered-reader-1+compression-deflate-dev (>= 1.3.0-~~),
 librust-buffered-reader-1-dev (>= 1.3.0-~~),
 librust-bzip2-0.4+default-dev,
 librust-camellia-0.1+default-dev,
 librust-camellia-0.1+zeroize-dev,
 librust-cast5-0.11+default-dev,
 librust-cast5-0.11+zeroize-dev,
 librust-cfb-mode-0.8+default-dev,
 librust-cipher-0.4+default-dev,
 librust-cipher-0.4+std-dev,
 librust-cipher-0.4+zeroize-dev,
 librust-des-0.8+default-dev,
 librust-des-0.8+zeroize-dev,
 librust-digest-0.10+default-dev,
 librust-dsa-0.6+default-dev,
 librust-dyn-clone-1+default-dev,
 librust-eax-0.5+default-dev,
 librust-ecb-0.1+default-dev,
 librust-ecdsa-0.16+arithmetic-dev,
 librust-ecdsa-0.16+default-dev,
 librust-ecdsa-0.16+hazmat-dev,
 librust-ed25519-2+std-dev,
 librust-flate2-1+default-dev (>= 1.0.1-~~),
 librust-idea-0.5+default-dev,
 librust-idea-0.5+zeroize-dev,
 librust-idna-0.4+default-dev,
 librust-lalrpop-0.20-dev,
 librust-lalrpop-util-0.20+default-dev,
 librust-lazy-static-1+default-dev (>= 1.4.0-~~),
 librust-libc-0.2+default-dev (>= 0.2.66-~~),
 librust-md-5-0.10+default-dev,
 librust-md-5-0.10+oid-dev,
 librust-memsec-dev (<< 0.7-~~),
 librust-nettle-7+default-dev (>= 7.3-~~),
 librust-num-bigint-dig-0.8-dev,
 librust-once-cell-1+default-dev,
 librust-openssl-0.10+default-dev (>= 0.10.55-~~),
 librust-openssl-sys-0.9+default-dev (>= 0.9.90-~~),
 librust-p256-0.13+default-dev,
 librust-p256-0.13+ecdh-dev,
 librust-p256-0.13+ecdsa-dev,
 librust-rand-0.8-dev,
 librust-rand-core-0.6+default-dev,
 librust-regex-1+default-dev,
 librust-regex-syntax-0.8+default-dev,
 librust-ripemd-0.1+default-dev,
 librust-ripemd-0.1+oid-dev,
 librust-sha1collisiondetection-0.3+std-dev (>= 0.3.1-~~),
 librust-sha2-0.10+default-dev,
 librust-sha2-0.10+oid-dev,
 librust-thiserror-1+default-dev (>= 1.0.2-~~),
 librust-twofish-0.7+default-dev,
 librust-twofish-0.7+zeroize-dev,
 librust-typenum-1+default-dev (>= 1.12.0-~~),
 librust-xxhash-rust-0.8+default-dev,
 librust-xxhash-rust-0.8+xxh3-dev
Provides:
 librust-sequoia-openpgp+--implicit-crypto-backend-for-tests-dev (= ${binary:Version}),
 librust-sequoia-openpgp+aes-dev (= ${binary:Version}),
 librust-sequoia-openpgp+aes-gcm-dev (= ${binary:Version}),
 librust-sequoia-openpgp+block-padding-dev (= ${binary:Version}),
 librust-sequoia-openpgp+blowfish-dev (= ${binary:Version}),
 librust-sequoia-openpgp+camellia-dev (= ${binary:Version}),
 librust-sequoia-openpgp+cast5-dev (= ${binary:Version}),
 librust-sequoia-openpgp+cfb-mode-dev (= ${binary:Version}),
 librust-sequoia-openpgp+cipher-dev (= ${binary:Version}),
 librust-sequoia-openpgp+compression-dev (= ${binary:Version}),
 librust-sequoia-openpgp+compression-bzip2-dev (= ${binary:Version}),
 librust-sequoia-openpgp+compression-deflate-dev (= ${binary:Version}),
 librust-sequoia-openpgp+crypto-botan-dev (= ${binary:Version}),
 librust-sequoia-openpgp+crypto-botan2-dev (= ${binary:Version}),
 librust-sequoia-openpgp+crypto-fuzzing-dev (= ${binary:Version}),
 librust-sequoia-openpgp+crypto-nettle-dev (= ${binary:Version}),
 librust-sequoia-openpgp+crypto-openssl-dev (= ${binary:Version}),
 librust-sequoia-openpgp+default-dev (= ${binary:Version}),
 librust-sequoia-openpgp+des-dev (= ${binary:Version}),
 librust-sequoia-openpgp+digest-dev (= ${binary:Version}),
 librust-sequoia-openpgp+dsa-dev (= ${binary:Version}),
 librust-sequoia-openpgp+eax-dev (= ${binary:Version}),
 librust-sequoia-openpgp+ecb-dev (= ${binary:Version}),
 librust-sequoia-openpgp+ecdsa-dev (= ${binary:Version}),
 librust-sequoia-openpgp+ed25519-dev (= ${binary:Version}),
 librust-sequoia-openpgp+idea-dev (= ${binary:Version}),
 librust-sequoia-openpgp+md-5-dev (= ${binary:Version}),
 librust-sequoia-openpgp+num-bigint-dig-dev (= ${binary:Version}),
 librust-sequoia-openpgp+p256-dev (= ${binary:Version}),
 librust-sequoia-openpgp+rand-dev (= ${binary:Version}),
 librust-sequoia-openpgp+rand-core-dev (= ${binary:Version}),
 librust-sequoia-openpgp+ripemd-dev (= ${binary:Version}),
 librust-sequoia-openpgp+sha2-dev (= ${binary:Version}),
 librust-sequoia-openpgp+twofish-dev (= ${binary:Version}),
 librust-sequoia-openpgp+typenum-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+--implicit-crypto-backend-for-tests-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+aes-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+aes-gcm-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+block-padding-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+blowfish-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+camellia-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+cast5-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+cfb-mode-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+cipher-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+compression-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+compression-bzip2-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+compression-deflate-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+crypto-botan-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+crypto-botan2-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+crypto-fuzzing-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+crypto-nettle-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+crypto-openssl-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+default-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+des-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+digest-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+dsa-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+eax-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+ecb-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+ecdsa-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+ed25519-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+idea-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+md-5-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+num-bigint-dig-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+p256-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+rand-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+rand-core-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+ripemd-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+sha2-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+twofish-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1+typenum-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+--implicit-crypto-backend-for-tests-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+aes-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+aes-gcm-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+block-padding-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+blowfish-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+camellia-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+cast5-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+cfb-mode-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+cipher-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+compression-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+compression-bzip2-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+compression-deflate-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+crypto-botan-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+crypto-botan2-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+crypto-fuzzing-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+crypto-nettle-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+crypto-openssl-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+default-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+des-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+digest-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+dsa-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+eax-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+ecb-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+ecdsa-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+ed25519-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+idea-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+md-5-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+num-bigint-dig-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+p256-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+rand-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+rand-core-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+ripemd-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+sha2-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+twofish-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19+typenum-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+--implicit-crypto-backend-for-tests-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+aes-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+aes-gcm-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+block-padding-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+blowfish-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+camellia-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+cast5-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+cfb-mode-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+cipher-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+compression-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+compression-bzip2-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+compression-deflate-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+crypto-botan-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+crypto-botan2-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+crypto-fuzzing-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+crypto-nettle-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+crypto-openssl-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+default-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+des-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+digest-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+dsa-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+eax-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+ecb-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+ecdsa-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+ed25519-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+idea-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+md-5-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+num-bigint-dig-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+p256-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+rand-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+rand-core-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+ripemd-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+sha2-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+twofish-dev (= ${binary:Version}),
 librust-sequoia-openpgp-1.19.0+typenum-dev (= ${binary:Version})
Description: OpenPGP data types and associated machinery - Rust source code
 Source code for Debianized Rust crate "sequoia-openpgp"

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tikv-jemalloc-sys
Upstream-Contact:
 Alex Crichton <alex@alexcrichton.com>
 Gonzalo Brito Gadeschi <gonzalobg88@gmail.com>
Source: https://github.com/tikv/jemallocator

Files: *
Copyright:
 2017-2019 Alex Crichton <alex@alexcrichton.com>
 2017-2019 Gonzalo Brito Gadeschi <gonzalobg88@gmail.com>
 The TiKV Project Developers
License: MIT or Apache-2.0

Files: jemalloc/*
Copyright: 2002-present Jason Evans <jasone@canonware.com>
           2007-2012 Mozilla Foundation
           2009-present Facebook, Inc.
License: BSD-2-Clause

Files: jemalloc/include/msvc_compat/C99/stdint.h
Copyright: 2006-2008 Alexander Chemeris
License: BSD-2-Clause-Chemeris
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. The name of the author may be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: jemalloc/bin/jeprof.in
Copyright: 1998-2007 Google Inc.
License: BSD-3-Clause-Google
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following disclaimer
 in the documentation and/or other materials provided with the
 distribution.
 * Neither the name of Google Inc. nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
Comment:
 Embeds the SVGPan library which is
 .
 Copyright: 2009-2010 Andrea Leofreddi <a.leofreddi@itcharm.com>
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 THIS SOFTWARE IS PROVIDED BY Andrea Leofreddi ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Andrea Leofreddi OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 The views and conclusions contained in the software and documentation
 are those of the authors and should not be interpreted as
 representing official policies, either expressed or implied, of
 Andrea Leofreddi.

Files: jemalloc/test/unit/hash.c jemalloc/test/unit/hash.h
Copyright: 2010-2012 Austin Appleby
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

Files: jemalloc/test/unit/SFMT.c jemalloc/test/src/SFMT.c jemalloc/test/include/test/SFMT-*
Copyright: 2006-2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima University
License: BSD-3-Clause-Hiroshima-University
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of the Hiroshima University nor the names of
 its contributors may be used to endorse or promote products
 derived from this software without specific prior written
 permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: configure/configure
Copyright:
 1992-1996, 1998-2012 Free Software Foundation, Inc.
 2012 Free Software Foundation, Inc.
 2008 Benjamin Kosnik <bkoz@redhat.com>
 2012 Zack Weinberg <zackw@panix.com>
 2013 Roy Stogner <roystgnr@ices.utexas.edu>
 2014, 2015 Google Inc.; contributed by Alexey Sokolov <sokolov@google.com>
 2015 Paul Norman <penorman@mac.com>
 2015 Moritz Klammler <moritz@klammler.eu>
 2016, 2018 Krzesimir Nowak <qdlacz@gmail.com>
 2019 Enji Cooper <yaneurabeya@gmail.com>
 2012 Free Software Foundation, Inc.
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/COPYING
Copyright:
 2002-present Jason Evans <jasone@canonware.com>.
 2007-2012 Mozilla Foundation.  All rights reserved.
 2009-present Facebook, Inc.  All rights reserved.
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/bin/jeprof.in
Copyright:
 1998-2007, Google Inc.
 1998-2007 Google Inc.
 2009-2010 Andrea Leofreddi <a.leofreddi@itcharm.com>. All rights reserved.
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/build-aux/config.guess
Copyright:
 1992-2021 Free Software Foundation, Inc.
 1992-2021 Free Software Foundation, Inc.
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/build-aux/config.sub
Copyright:
 1992-2021 Free Software Foundation, Inc.
 1992-2021 Free Software Foundation, Inc.
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/build-aux/install-sh
Copyright: 1991 by the Massachusetts Institute of Technology
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/include/msvc_compat/C99/stdint.h
Copyright: 2006-2008 Alexander Chemeris
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/m4/ax_cxx_compile_stdcxx.m4
Copyright:
 2008 Benjamin Kosnik <bkoz@redhat.com>
 2012 Zack Weinberg <zackw@panix.com>
 2013 Roy Stogner <roystgnr@ices.utexas.edu>
 2014, 2015 Google Inc.; contributed by Alexey Sokolov <sokolov@google.com>
 2015 Paul Norman <penorman@mac.com>
 2015 Moritz Klammler <moritz@klammler.eu>
 2016, 2018 Krzesimir Nowak <qdlacz@gmail.com>
 2019 Enji Cooper <yaneurabeya@gmail.com>
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-alti.h
Copyright:
 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
 2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params11213.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params1279.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params132049.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params19937.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params216091.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params2281.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params4253.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params44497.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params607.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-params86243.h
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT-sse2.h
Copyright:
 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
 2006, 2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/include/test/SFMT.h
Copyright:
 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
 2006, 2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/src/SFMT.c
Copyright:
 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/unit/SFMT.c
Copyright: 2006,2007 Mutsuo Saito, Makoto Matsumoto and Hiroshima
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: jemalloc/test/unit/hash.c
Copyright: 2010-2012 Austin Appleby
License: UNKNOWN-LICENSE; FIXME (overlay)
Comment:
 FIXME (overlay): These notices are extracted from files. Please review them
 before uploading to the archive.

Files: debian/*
Copyright:
 2019-2023 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2019 Sylvestre Ledru <sylvestre@debian.org>
 2023 Jelmer Vernooĳ <jelmer@debian.org>
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
